package com.itheima.pojo;

public class Customer {
	private Integer id;
	private String userName;
	private String jobs;
	private String phone;
	public Integer getId() {
		return id;
	}
	public String getUserName() {
		return userName;
	}
	public String getJobs() {
		return jobs;
	}
	public String getPhone() {
		return phone;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setJobs(String jobs) {
		this.jobs = jobs;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "Customer [id=" + id + ", userName=" + userName + ", jobs=" + jobs + ", phone=" + phone + "]";
	}
	
}
