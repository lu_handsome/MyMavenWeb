package com.itheima.pojo;

public class User {
	private String username;
	private Integer id;
	private String address;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "User{" +
				"username='" + username + '\'' +
				", id=" + id +
				", address='" + address + '\'' +
				'}';
	}
}
