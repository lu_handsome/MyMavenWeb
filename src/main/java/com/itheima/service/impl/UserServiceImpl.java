package com.itheima.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itheima.mapper.CustomerMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.pojo.Customer;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserMapper usermapper;
	
	@Override
	public User selectUser(String name) {
		// TODO Auto-generated method stub
		return usermapper.selectUser(name);
	}

}
