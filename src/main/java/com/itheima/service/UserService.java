package com.itheima.service;

import org.springframework.stereotype.Service;

import com.itheima.pojo.Customer;
import com.itheima.pojo.User;


public interface UserService {
	public User selectUser(String name);
}
