package com.itheima.controller;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itheima.pojo.User;
import com.itheima.service.UserService;
import com.sun.org.apache.regexp.internal.recompile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

@Controller
public class FirstController{
	@Autowired
	UserService userservice;

	@RequestMapping("/login")
	public String login(User user,Model model) {
		model.addAttribute(user);
		System.out.println(userservice.selectUser(user.getUsername()));
		return "success";
	}
	@RequestMapping("/testdata")
	@ResponseBody
	public String testdata(){
		String data="data";
		String test="test";
		return  data;
	}
}
