package com.itheima.mapper;

import com.itheima.pojo.Customer;
import com.itheima.pojo.User;

public interface CustomerMapper {
	public Customer findCustomerById(Integer id);
}
