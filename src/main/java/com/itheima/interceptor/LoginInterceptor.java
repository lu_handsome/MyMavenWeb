package com.itheima.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.itheima.pojo.User;

public class LoginInterceptor implements HandlerInterceptor{

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		System.out.println("afterCompletion.....处理");
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		System.out.println("postHandle.....处理");
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response , Object Handler) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("preHandle.....拦截器处理");
		
//		String url =request.getRequestURI();
//		if (url.indexOf("/login")>=0) {
//			return true;
//		}
//		HttpSession session =request.getSession();
//		User user =(User) session.getAttribute("USER_SESSION");
//		if (user!=null) {
//			return true;
//		}else {
//			request.setAttribute("msg", "您还没有登录，请先登录");
//			request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
//		}
//		return false;
		
		return true;
	}

}
